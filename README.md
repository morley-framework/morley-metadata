> :warning: **Note: this project is deprecated.**
>
> It is no longer maintained since the activation of protocol "Nairobi" on the Tezos mainnet (June 24th, 2023).

# morley-metadata

Types and helpers to implement a TZIP-16 (metadata) compliant smart contract using the Morley framework.