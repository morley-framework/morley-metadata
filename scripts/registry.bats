#!/usr/bin/env bats

# Tests for morley-metadata-registry

# SPDX-FileCopyrightText: 2020 TQ Tezos
# SPDX-License-Identifier: LicenseRef-MIT-TQ

exe="morley-metadata-registry"
# We expect all these contracts to be present.
contracts="MetadataCarrier"

@test "Invoking with --help" {
  "$exe" --help
}

@test "Invoking with --version" {
  "$exe" --version
}

@test "List available contracts" {
  list=`"$exe" list`
  for name in $contracts;
  do
    echo "$list" | grep "$name"
  done
}

@test "Print contracts" {
  for name in $contracts;
  do
    "$exe" print -n "$name"
  done
}

@test "%metadata annotation" {
  "$exe" print --name MetadataCarrier --oneline -o - | grep "big_map %metadata string bytes"
}
