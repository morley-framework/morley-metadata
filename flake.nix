# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

{
  description = "The morley-metadata flake";

  nixConfig.flake-registry = "https://gitlab.com/morley-framework/morley-infra/-/raw/main/flake-registry.json";

  inputs.morley-infra.url = "gitlab:morley-framework/morley-infra";

  outputs = { self, flake-utils, morley-infra, ... }:
    (flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        pkgs = morley-infra.legacyPackages.${system};

        inherit (morley-infra.utils.${system}) ci-apps;

        # all local packages and their subdirectories
        # we need to know subdirectories for weeder and for cabal check
        local-packages = [
          { name = "morley-metadata"; subdirectory = "./code/morley-metadata"; }
          { name = "morley-metadata-test"; subdirectory = "./code/morley-metadata-test"; }
        ];

        # names of all local packages
        local-packages-names = map (p: p.name) local-packages;

        # source with gitignored files filtered out
        projectSrc = pkgs.haskell-nix.haskellLib.cleanGit {
          name = "morley-metadata";
          src = ./.;
        };

        # haskell.nix package set
        hs-pkgs = pkgs.haskell-nix.stackProject {
          src = projectSrc;

          # use .cabal files for building because:
          # 1. haskell.nix fails to work for package.yaml with includes from the parent directory
          # 2. with .cabal files haskell.nix filters out source files for each component, so only the changed components will rebuild
          ignorePackageYaml = true;

          modules = [
            # common options for all local packages:
            {
              packages = pkgs.lib.genAttrs local-packages-names (packageName: ci-apps.collect-hie false {
                ghcOptions = [ "-O0" "-Werror" ];

                # enable haddock for local packages
                doHaddock = true;
              });
            }

            {
              # don't haddock dependencies
              doHaddock = false;
            }
          ];

          shell = {
            tools = {
              cabal = {};
              hlint = { version = "3.5"; };
              hpack = { version = "0.35.1"; };
            };
          };
        };

        # We don't distinguish development and non-development modes for now.
        hs-pkgs-development = hs-pkgs;

        flake = hs-pkgs-development.flake {};

      in pkgs.lib.lists.foldr pkgs.lib.recursiveUpdate {} [

        { inherit (flake) packages devShells apps; }
        {
          utils = { inherit (morley-infra.utils.${system}) run-chain-tests; };

          legacyPackages = pkgs;

          packages = {
            all-components = pkgs.linkFarmFromDrvs "all-components" (builtins.attrValues flake.packages);

            default = self.packages.${system}.all-components;
          };

          checks = {
            # checks if all packages are appropriate for uploading to hackage
            run-cabal-check = morley-infra.utils.${system}.run-cabal-check { inherit local-packages projectSrc; };

            trailing-whitespace = pkgs.build.checkTrailingWhitespace ./.;

            reuse-lint = pkgs.build.reuseLint ./.;
          };

          apps = ci-apps.apps {
            hs-pkgs = hs-pkgs-development;
            inherit local-packages projectSrc;
          };
        }
      ]));
}
