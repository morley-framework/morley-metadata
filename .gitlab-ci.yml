# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

variables:
  TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER: "Y"
  CHAIN_TEST_MONEYBAG_SECRET_KEY: "unencrypted:edsk3AvAoPS5jFov49aWBQ9oVtCPwNSne2CriHvkFxfeCg4Srr5bak"
  # this key is defined in local-chain bootstrap accounts list in
  # https://github.com/serokell/aquarius-infra/blob/master/servers/albali/chain.nix

.nairobinet-variables: &nairobinet-variables
  variables:
    TASTY_CLEVELAND_NODE_ENDPOINT: "https://nairobi.testnet.tezos.serokell.team"
    CHAIN_TEST_MONEYBAG_SECRET_KEY: "$TESTNET_MONEYBAG"
    CHAIN_TEST_MONEYBAG_PASSWORD: "$TESTNET_MONEYBAG_PASSWORD"

.test-report: &test-report
  after_script:
      # edit xml report due to bug with `classname` field in test xml reports in CI
      # https://gitlab.com/gitlab-org/gitlab-ce/issues/52997
      - sed -i -r
          's:<testcase name="([^<>]*?)" time="([0-9.]*?)" classname="([^<>]*?)">:<testcase name="\3.\1" time="\2" classname="\3">:g'
          test-report.xml
  artifacts:
    reports:
      junit:
        - test-report.xml

.network-tests: &network-tests
  script:
    - nix build -L .#morley-metadata-test:test:morley-metadata-test
    - $(nix-build . -A utils.run-chain-tests
        --argstr refill-balance 10
        --argstr node-endpoint "$TASTY_CLEVELAND_NODE_ENDPOINT"
        --argstr step-moneybag "$CHAIN_TEST_MONEYBAG_SECRET_KEY"
        --argstr step-moneybag-password "$CHAIN_TEST_MONEYBAG_PASSWORD"
        --no-out-link
        --argstr scenario './result/bin/morley-metadata-test
          --color=always
          --cleveland-mode=only-network
          --xml=./test-report.xml')
  retry: 1

stages:
  - validate
  - build
  - test
  - test-extras

default:
  tags: [shell-executor]

verify-doc-links:
  stage: validate
  only: [merge_requests]
  allow_failure: true
  retry:
    max: 2
    when: script_failure
  script:
    - nix shell .#xrefcheck --impure -c
        xrefcheck -v --no-progress -m full

validate-cabal-files:
  stage: validate
  only: [merge_requests]
  script:
    - nix shell .#stack2cabal -c
        ./scripts/ci/validate-cabal-files.sh ./examples/EDSL/edsl-demo.cabal

# it would be better to run this inside nix-build for isolation
lint:
  stage: validate
  only: [merge_requests]
  script:
    - nix shell .#haskellPackages.hlint -c
        ./scripts/lint.sh

check:
  stage: validate
  only: [merge_requests]
  script:
    - nix flake check -L

shellcheck-scripts:
  stage: validate
  only: [merge_requests]
  script:
    - nix shell .#shellcheck -c find . -name '*.sh' -exec shellcheck {} +

# note: ci relies on .cabal files, not package.yaml
build-all:
  stage: build
  only: [merge_requests, master]
  timeout: 3h
  script:
    - nix build -L .#all-components

# tests are run outside nix-build to produce the test report
test-morley-metadata:
  stage: test
  only: [merge_requests, master]
  script:
    - nix build -L .#morley-metadata:test:morley-metadata-test
    - ./result/bin/morley-metadata-test
        --xml=test-report.xml
  <<: *test-report

test-morley-metadata-test:
  stage: test
  only: [merge_requests, master]
  script:
    - nix build -L .#morley-metadata-test:test:morley-metadata-test
    - ./result/bin/morley-metadata-test
        --xml=test-report.xml
        --cleveland-mode=disable-network
  <<: *test-report

test-morley-metadata-test-local-chain-017:
  stage: test
  only: [merge_requests, master]
  variables:
    TASTY_CLEVELAND_NODE_ENDPOINT: "http://localhost:8734"
  <<: *network-tests
  <<: *test-report

test-morley-metadata-test-nairobinet:
  stage: test
  only:
    refs: [schedules]
    variables: [$SCHEDULED_NETWORK_TESTS]
  <<: *network-tests
  <<: *test-report
  <<: *nairobinet-variables

test-morley-metadata-registry:
  stage: test
  only: [merge_requests, master]
  script:
    - nix shell .#bats .#morley-metadata:exe:morley-metadata-registry
        -c bats ./scripts/registry.bats

weeder:
  stage: test-extras
  only: [merge_requests]
  script:
    - nix run -L .#ci:weeder
