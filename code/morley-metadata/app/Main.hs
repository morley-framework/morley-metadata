-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Main
  ( main
  ) where

import Data.Map qualified as Map
import Data.Version (showVersion)
import Options.Applicative qualified as Opt
import Options.Applicative.Help.Pretty (Doc, linebreak)
import Paths_morley_metadata (version)

import Lorentz (DGitRevision(..))
import Lorentz.ContractRegistry
import Lorentz.Contracts.MetadataCarrier (metadataCarrierContract)
import Morley.Util.Main (wrapMain)

programInfo :: Opt.ParserInfo CmdLnArgs
programInfo =
  -- git revision doesn't matter because there is no documentation
  Opt.info (Opt.helper <*> versionOption <*> argParser contracts DGitRevisionUnknown) $
  mconcat
  [ Opt.fullDesc
  , Opt.progDesc "Metadata contracts registry"
  , Opt.header "Metadata contracts for Michelson"
  , Opt.footerDoc usageDoc
  ]
  where
    versionOption = Opt.infoOption ("morley-metadata-" <> showVersion version)
      (Opt.long "version" <> Opt.help "Show version.")

usageDoc :: Maybe Doc
usageDoc = Just $ mconcat
   [ "You can use help for specific COMMAND", linebreak
   , "EXAMPLE:", linebreak
   , "  morley-metadata print --help", linebreak
   ]

contracts :: ContractRegistry
contracts = ContractRegistry $ Map.fromList
  [ "MetadataCarrier" ?:: ContractInfo
      { ciContract = metadataCarrierContract
      , ciIsDocumented = False
      , ciStorageParser = Nothing
      , ciStorageNotes = Nothing
      }
  ]

main :: IO ()
main = wrapMain $ do
  cmdLnArgs <- Opt.execParser programInfo
  runContractRegistry contracts cmdLnArgs
