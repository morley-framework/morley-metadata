-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | A simple contract which only purpose is storing metadata.
module Lorentz.Contracts.MetadataCarrier
  ( Storage (..)
  , mkStorage
  , metadataCarrierContract
  , metadataCarrierContractCarnage
  ) where

import Lorentz

import Lorentz.Contracts.Spec.TZIP16Interface
import Morley.Michelson.Typed.Haskell.Doc (FieldCamelCase)

data Storage = Storage
  { sMetadata :: MetadataMap
    -- ^ Metadata itself
  , sDummy :: ()
    -- ^ An extra field, without it we couldn't attach a field annotation
  } deriving stock (Show, Generic)
    deriving anyclass (IsoValue, HasAnnotation)
    deriving TypeHasFieldNamingStrategy via FieldCamelCase

mkStorage :: MetadataMap -> Storage
mkStorage = flip Storage ()

metadataCarrierContract :: Contract Never Storage ()
metadataCarrierContract = metadataCarrierContractCarnage

-- | Version of 'metadataCarrierContract' that should become
-- obsolete in Edo, such contract can be called but still always fails.
metadataCarrierContractCarnage :: Contract Never Storage ()
metadataCarrierContractCarnage = defaultContract $ car # never
