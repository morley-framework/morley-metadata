-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | TZIP-16 specification.
-- https://gitlab.com/tzip/tzip/-/blob/27e556c0ca7f13acbf0eacea354f426447f02f18/proposals/tzip-16/tzip-16.md
module Lorentz.Contracts.Spec.TZIP16Interface
  ( MetadataMap
  , MetadataMapId
  , metadataURI
  -- * Metadata
  , Metadata(..)
  , name
  , getName
  , description
  , getDescription
  , version
  , getVersion
  , license
  , getLicense
  , authors
  , getAuthors
  , homepage
  , getHomepage
  , source
  , getSource
  , interfaces
  , getInterfaces
  , errors
  , getErrors
  , views
  , getViews

  -- * Standard metadata components
  , Author(..)
  , author
  , Interface(..)
  , tzip
  , tzipWithExtras
  , License(..)
  , Source(..)
  , Error(..)
  , StaticError(..)
  , DynamicError(..)
  , View(..)
  , ViewImplementation(..)
  , MichelsonStorageView(..)
  , AnnotationInfo(..)
  , MichelsonVersion(..)
  , carthageVersion
  , delphiVersion
  , edoVersion
  , florenceVersion
  , granadaVersion
  , hangzhouVersion
  , ithacaVersion
  , jakartaVersion
  , RestApiQuery(..)
  , RestMethod(..)

  , module TZIP16.URI

  -- * Helpers
  , runParser
  , keyValue
  ) where

import Data.Aeson hiding (One)
import Data.Aeson.Key qualified as K
import Data.Aeson.KeyMap qualified as KM
import Data.Aeson.TH (deriveJSON)
import Data.Aeson.Text qualified as J
import Data.Aeson.Types (Parser, parseEither)
import Data.Coerce (coerce)
import Fmt (Buildable(..), pretty)

import Lorentz (BigMap, MText)
import Morley.AsRPC (AsRPC)
import Morley.Micheline (Expression)

import Lorentz.Contracts.Spec.TZIP16Interface.URI as TZIP16.URI
import Morley.Metadata.Util.Aeson (aesonOptions)

{-# ANN module ("HLint: ignore Avoid lambda using `infix`" :: Text) #-}
{-# ANN module ("HLint: ignore Use list comprehension" :: Text) #-}

----------------------------------------------------------------------------
-- ADTs
----------------------------------------------------------------------------

-- | TZIP-16 Metadata big_map.
--
-- See: <https://gitlab.com/tzip/tzip/-/blob/27e556c0ca7f13acbf0eacea354f426447f02f18/proposals/tzip-16/tzip-16.md#contract-storage>
type MetadataMap = BigMap MText ByteString
type MetadataMapId = AsRPC MetadataMap

-- | Construct a piece of map containing a reference to the contract's metadata.
metadataURI :: URI -> MetadataMap
metadataURI uri = one (mempty, encodeURI uri)

-- | TZIP-16 Metadata JSON.
--
-- See: <https://gitlab.com/tzip/tzip/-/blob/27e556c0ca7f13acbf0eacea354f426447f02f18/proposals/tzip-16/tzip-16.md#metadata-json-format>
newtype Metadata st = Metadata Object
  deriving stock (Eq, Show)
  deriving newtype (FromJSON, ToJSON, Semigroup, Monoid)

instance Buildable (Metadata st) where
  build (Metadata obj) = build $ J.encodeToTextBuilder obj

-- | Parse a value from the given metadata.
--
-- TZIPs that extend TZIP-16 may use this function to implement their own metadata parsers.
runParser :: forall a st. (Object -> Parser a) -> Metadata st -> Either String a
runParser f (Metadata obj) = parseEither f obj

-- | Creates a metadata using a single key-value pair.
-- To compose multiple key-value pairs, use 'Metadata' 's 'Monoid' instance, e.g.:
--
-- > keyValue @Int "count" 1 <> keyValue @Int "amount" 2
--
-- TZIPs that extend TZIP-16 may use this function to implement their own metadata builders.
keyValue :: ToJSON val => Key -> val -> Metadata st
keyValue k v =
  Metadata . KM.fromList $
    if valueJSON /= Array mempty
      then [k .= valueJSON]
      else []
  where
    valueJSON = toJSON v

----------------------------------------------------------------------------
-- Standard metadata components
----------------------------------------------------------------------------

-- | The name and contract of a contract's author.
--
-- It's recommended to use the 'author' function to ensure the
-- author's name and contact are properly formatted.
newtype Author = Author Text
  deriving stock (Eq, Show)
  deriving newtype (FromJSON, ToJSON)

author
  :: Text -- ^ The author's name.
  -> Text -- ^ The author's contact.
  -> Author
author n c = Author $
  n <> " <" <> c <> ">"

-- | A interface that this contract claims to obey.
--
-- It's recommended to use the 'tzip' and 'tzipWithExtras' functions
-- to ensure the
-- author's name and contact are properly formatted.
newtype Interface = Interface Text
  deriving stock (Eq, Show)
  deriving newtype (FromJSON, ToJSON)

-- | Construct an 'Interface' from a TZIP number.
--
-- > tzip 12 == "TZIP-12"
tzip :: Natural -> Interface
tzip tzipNumber = Interface $
  "TZIP-" <> show tzipNumber

-- | Construct an 'Interface' from a TZIP number and some additional information
--
-- > tzipWithExtras 12 "git 6544de32" == "TZIP-12 git 6544de32"
tzipWithExtras :: Natural -> Text -> Interface
tzipWithExtras tzipNumber extras = Interface $
  coerce (tzip tzipNumber) <> " " <> extras

data License = License
  { lName :: Text
  , lDetails :: Maybe Text
  }
  deriving stock (Eq, Show)

data Source = Source
  { sLocation :: Maybe Text
  , sTools :: [Text]
  }
  deriving stock (Eq, Show, Generic)

data Error
  = EStatic StaticError
  | EDynamic DynamicError
  deriving stock (Eq, Show)

data StaticError = StaticError
  { seError :: Expression
  , seExpansion :: Expression
  , seLanguages :: [Text]
  }
  deriving stock (Eq, Show, Generic)

data DynamicError = DynamicError
  { deView :: Text
  , deLanguages :: [Text]
  }
  deriving stock (Eq, Show, Generic)

data View st = View
  { vName :: Text
  , vDescription :: Maybe Text
  , vPure :: Maybe Bool
  , vImplementations :: [ViewImplementation st]
  }
  deriving stock (Eq, Show, Generic)

data ViewImplementation st
  = VIMichelsonStorageView (MichelsonStorageView st)
  | VIRestApiQuery RestApiQuery
  deriving stock (Eq, Show)

-- | A view object that requires access to the contract's storage in order to run,
-- as defined by TZIP-16.
--
-- See: <https://gitlab.com/tzip/tzip/-/blob/27e556c0ca7f13acbf0eacea354f426447f02f18/proposals/tzip-16/tzip-16.md#michelson-storage-views>
data MichelsonStorageView st = MichelsonStorageView
  { msvParameter :: Maybe Expression
  , msvReturnType :: Expression
  , msvCode :: Expression
  , msvAnnotations :: [AnnotationInfo]
  , msvVersion :: Maybe MichelsonVersion
  }
  deriving stock (Eq, Show, Generic)

-- | A string representing the version of Michelson that the view is meant to work with.
-- Michelson versions are base58check-encoded protocol hashes.
newtype MichelsonVersion = MichelsonVersion Text
  deriving stock (Eq, Show)
  deriving newtype (FromJSON, ToJSON)

-- Note: protocol hashes can be found in: https://gitlab.com/tezos/tezos/-/tree/
-- In the file `src/proto_<version>/lib_protocol/TEZOS_PROTOCOL`

carthageVersion :: MichelsonVersion
carthageVersion = MichelsonVersion "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb"

delphiVersion :: MichelsonVersion
delphiVersion = MichelsonVersion "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo"

edoVersion :: MichelsonVersion
edoVersion = MichelsonVersion "PtEdoTezd3RHSC31mpxxo1npxFjoWWcFgQtxapi51Z8TLu6v6Uq"

florenceVersion :: MichelsonVersion
florenceVersion = MichelsonVersion "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i"

granadaVersion :: MichelsonVersion
granadaVersion = MichelsonVersion "PtGRANADsDU8R9daYKAgWnQYAJ64omN1o3KMGVCykShA97vQbvV"

hangzhouVersion :: MichelsonVersion
hangzhouVersion = MichelsonVersion "PtHangz2aRngywmSRGGvrcTyMbbdpWdpFKuS4uMWxg2RaH9i1qx"

ithacaVersion :: MichelsonVersion
ithacaVersion = MichelsonVersion "Psithaca2MLRFYargivpo7YvUr7wUDqyxrdhC5CQq78mRvimz6A"

jakartaVersion :: MichelsonVersion
jakartaVersion = MichelsonVersion "PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY"

data AnnotationInfo = AnnotationInfo
  { aName :: Text
  , aDescription :: Text
  }
  deriving stock (Eq, Show)

-- | Describes how to map the view to an Open API description of a REST-API.
--
-- See: <https://gitlab.com/tzip/tzip/-/blob/27e556c0ca7f13acbf0eacea354f426447f02f18/proposals/tzip-16/tzip-16.md#rest-api-views>
data RestApiQuery = RestApiQuery
  { ravSpecificationUri :: Text
  , ravBaseUri :: Maybe Text
  , ravPath :: Text
  , ravMethod :: Maybe RestMethod
  }
  deriving stock (Eq, Show)

data RestMethod = GET | POST | PUT
  deriving stock (Eq, Show, Enum, Bounded)

----------------------------------------------------------------------------
-- JSON serializers/deserializers
----------------------------------------------------------------------------

-- | Removes a specific field from a JSON object if the value is an empty array.
omitEmptyArrayField :: Key -> Value -> Value
omitEmptyArrayField keyName = \case
  Object obj -> Object $ KM.filterWithKey (\k v -> k /= keyName || v /= Array mempty) obj
  val -> val

-- | If the given object does not contain the given key, insert it into
-- the object with an empty array.
insertEmptyArrayField :: Key -> Value -> Value
insertEmptyArrayField keyName v =
  case v of
    Object obj
      | KM.member keyName obj -> v
      | otherwise -> Object $ KM.insert keyName (Array mempty) obj
    _ -> v

deriveJSON aesonOptions ''License
deriveJSON aesonOptions ''AnnotationInfo
deriveJSON aesonOptions ''RestMethod
deriveJSON aesonOptions ''RestApiQuery
deriveJSON aesonOptions { sumEncoding = UntaggedValue } ''Error

instance ToJSON (MichelsonStorageView st) where
  toJSON = omitEmptyArrayField "annotations" . genericToJSON aesonOptions

instance FromJSON (MichelsonStorageView st) where
  parseJSON = genericParseJSON aesonOptions . insertEmptyArrayField "annotations"

instance ToJSON StaticError where
  toJSON = omitEmptyArrayField "languages" . genericToJSON aesonOptions

instance FromJSON DynamicError where
  parseJSON = genericParseJSON aesonOptions . insertEmptyArrayField "languages"

instance FromJSON StaticError where
  parseJSON = genericParseJSON aesonOptions . insertEmptyArrayField "languages"

instance ToJSON DynamicError where
  toJSON = omitEmptyArrayField "languages" . genericToJSON aesonOptions

deriveJSON aesonOptions ''View

instance ToJSON Source where
  toJSON = omitEmptyArrayField "tools" . genericToJSON aesonOptions

instance FromJSON Source where
  parseJSON = genericParseJSON aesonOptions . insertEmptyArrayField "tools"

instance ToJSON (ViewImplementation st) where
  toJSON = \case
    VIMichelsonStorageView msv -> object [ "michelsonStorageView" .= msv ]
    VIRestApiQuery raq -> object [ "restApiQuery" .= raq ]

instance FromJSON (ViewImplementation st) where
  parseJSON = withObject "ViewImplementation" $ \obj -> do
    case (KM.lookup "michelsonStorageView" obj, KM.lookup "restApiQuery" obj) of
      (Just msvValue, _) -> VIMichelsonStorageView @st <$> parseJSON msvValue
      (_, Just raqValue) -> VIRestApiQuery  <$> parseJSON raqValue
      _ -> fail . mconcat $
        [ "Only 'michelsonStorageView' and 'restApiQuery' view implementations are supported. "
        , "Found view implementation with keys: "
        , pretty (K.toText <$> KM.keys obj)
        ]

----------------------------------------------------------------------------
-- Accessors
----------------------------------------------------------------------------

name :: Text -> Metadata st
name = keyValue "name"

getName :: Metadata st -> Either String (Maybe Text)
getName = runParser \o -> o .:? "name"

description :: Text -> Metadata st
description = keyValue "description"

getDescription :: Metadata st -> Either String (Maybe Text)
getDescription = runParser \o -> o .:? "description"

version :: Text -> Metadata st
version = keyValue "version"

getVersion :: Metadata st -> Either String (Maybe Text)
getVersion = runParser \o -> o .:? "version"

license :: License -> Metadata st
license = keyValue "license"

getLicense :: Metadata st -> Either String (Maybe License)
getLicense = runParser \o -> o .:? "license"

authors :: [Author] -> Metadata st
authors = keyValue "authors"

getAuthors :: Metadata st -> Either String [Author]
getAuthors = runParser \o -> o .:? "authors" .!= []

homepage :: Text -> Metadata st
homepage = keyValue "homepage"

getHomepage :: Metadata st -> Either String (Maybe Text)
getHomepage = runParser \o -> o .:? "homepage"

source :: Source -> Metadata st
source = keyValue "source"

getSource :: Metadata st -> Either String (Maybe Source)
getSource = runParser \o -> o .:? "source"

interfaces :: [Interface] -> Metadata st
interfaces = keyValue "interfaces"

getInterfaces :: Metadata st -> Either String [Interface]
getInterfaces = runParser \o -> o .:? "interfaces" .!= []

errors :: [Error] -> Metadata st
errors = keyValue "errors"

getErrors :: Metadata st -> Either String [Error]
getErrors = runParser \o -> o .:? "errors" .!= []

views :: [View st] -> Metadata st
views = keyValue "views"

getViews :: Metadata st -> Either String [View st]
getViews = runParser \o -> o .:? "views" .!= []
