-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | URIs in TZIP-16 specification.
-- <https://gitlab.com/tzip/tzip/-/blob/27e556c0ca7f13acbf0eacea354f426447f02f18/proposals/tzip-16/tzip-16.md>
module Lorentz.Contracts.Spec.TZIP16Interface.URI
  ( URI (..)
  , encodeURI

  -- * References to contract metadata
  , tezosStorageScheme
  , tezosStorageUri
  , ContractHost (..)
  , ExtChainId (..)
  , selfHost
  , contractHost
  , foreignContractHost

    -- * Helpers
  , uriEncodePathSegment
  ) where

import Fmt (Buildable(..), pretty)
import Network.URI.Encode qualified as URI

import Morley.Michelson.Text
import Morley.Tezos.Address
import Morley.Tezos.Core

-- | URIs in metadata.
--
-- See: <https://gitlab.com/tzip/tzip/-/blob/27e556c0ca7f13acbf0eacea354f426447f02f18/proposals/tzip-16/tzip-16.md#metadata-uris>
newtype URI = URI { unURI :: Text }
  deriving stock (Eq, Ord, Show)
  deriving newtype (IsString)

-- | Compile a URI to a value in metadata map.
encodeURI :: URI -> ByteString
encodeURI =
  -- One might reasonably expect that the URI would be stored as packed
  -- Michelson strings, but the TZIP-16 spec is explicit about that
  -- not being the case.
  --
  -- > Unless otherwise-specified, the encoding of the values must be the direct stream
  -- > of bytes of the data being stored. (...)
  -- > There is no implicit conversion to Michelson's binary format (PACK) nor
  -- > quoting mechanism.
  --
  -- See: <https://gitlab.com/tzip/tzip/-/blob/27e556c0ca7f13acbf0eacea354f426447f02f18/proposals/tzip-16/tzip-16.md#contract-storage>
  --
  -- So, instead, we encode it as UTF-8 byte sequences.
  encodeUtf8 . unURI

-- | References a contract.
--
-- See: <https://gitlab.com/tzip/tzip/-/blob/27e556c0ca7f13acbf0eacea354f426447f02f18/proposals/tzip-16/tzip-16.md#the-tezos-storage-uri-scheme>
newtype ContractHost = ContractHost
  { unContractHost :: Maybe Text
    -- ^ 'Nothing' for the current contract, contract coordinates otherwise.
  }
  deriving stock (Eq, Ord, Show)

data ExtChainId
  = ChainByName Text
  | ChainById ChainId
  deriving stock (Eq, Show, Generic)

instance IsString ExtChainId where
  fromString = ChainByName . fromString

instance Buildable ExtChainId where
  build = \case
    ChainByName nm -> build nm
    ChainById chainId -> build chainId

-- | Construct a reference to the current contract.
selfHost :: ContractHost
selfHost = ContractHost Nothing

-- | Construct a reference to the given contract.
contractHost :: ContractAddress -> ContractHost
contractHost address = ContractHost $ Just (pretty address)

-- | Construct a reference to the given contract in another chain.
foreignContractHost :: ExtChainId -> ContractAddress -> ContractHost
foreignContractHost chainId address =
  ContractHost $ Just (pretty address <> "." <> pretty chainId)

uriEncodePathSegment :: Text -> Text
uriEncodePathSegment =
  -- We want to avoid escaping unnecessary characters for the sake prettier
  -- results, because e.g. sha256 type of URI includes other URIs into its
  -- path segment and thus requires escaping them.
  -- The default 'URI.encode' does percent-encoding too agressively.
  --
  -- Some relevant info:
  -- https://stackoverflow.com/a/4669755/4684686
  URI.encodeTextWith $ \c -> URI.isAllowed c || c `elem` [':', '@']

-- | Make a URI refering to a metadata in some contract's storage.
tezosStorageUri :: ContractHost -> MText -> URI
tezosStorageUri (ContractHost mHost) rawPath =
  URI case mHost of
    Nothing -> tezosStorageScheme <> ":" <> path
    Just host -> tezosStorageScheme <> "://" <> host <> "/" <> path
  where
    path = uriEncodePathSegment $ toText rawPath

tezosStorageScheme :: Text
tezosStorageScheme = "tezos-storage"
