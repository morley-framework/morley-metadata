-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Morley.Metadata.Util.Aeson
  ( aesonOptions
  ) where

import Data.Aeson (Options(omitNothingFields))
import Data.Aeson.Casing (aesonPrefix, camelCase)

aesonOptions :: Options
aesonOptions = (aesonPrefix camelCase) { omitNothingFields = True}
