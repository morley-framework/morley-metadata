-- SPDX-FileCopyrightText: 2022 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | This module provides utilities for constructing TZIP-16 @errors@ field of
-- the metadata structure from a Lorentz contract using autodoc.
module Morley.Metadata.Util.Errors
  ( errorsFromAutodoc
  , errorsFromAutodocTH
  ) where

import Lorentz
  (Contract, DError(..), MText, SomeDocDefinitionItem(..), buildDoc, cdDefinitionsSet,
  errorDocMdCause, errorDocName, finalizedAsIs, toVal)

import Control.Lens (each)
import Data.Coerce (coerce)
import Data.Typeable (cast)
import Data.Validation (Validation(..), liftError, toEither)
import Fmt (fmt)
import Language.Haskell.TH (Code(..), Q)

import Morley.Micheline (toExpression)
import Morley.Michelson.Text (mkMText, pattern UnsafeMText)

import Lorentz.Contracts.Spec.TZIP16Interface

-- | Construct TZIP-16 error descriptions from a Lorentz contract using autodoc.
--
-- See 'errorsFromAutodocTH' for a version that does checks at compile-time.
errorsFromAutodoc :: Contract st pt vd -> Either [Text] [Error]
errorsFromAutodoc =
  (fmap . fmap) makeTZ16Error . toEither . validateErrors . collectErrors

-- | Construct TZIP-16 error descriptions from a Lorentz contract using autodoc,
-- validating at compile-time.
--
-- This uses Michelson strings to represent both error names and descriptions.
-- Consequently, only a subset of ASCII can be used. If an invalid character
-- is found, a compile-time error is raised.
--
-- Example:
--
-- @
-- 'errors' $$(errorsFromAutodocTH myContract)
-- @
errorsFromAutodocTH :: Contract st pt vd -> Code Q [Error]
errorsFromAutodocTH contract = case validateErrors $ collectErrors contract of
  -- we just validated, so it's safe to use 'UnsafeMText'
  -- the reason we have to do this is that 'MText' doesn't have a 'Lift' instance
  -- Note we don't want to use 'coerce' in the TH splice because it will fail at
  -- the use site if 'UnsafeMText' is out of scope.
  Success (coerce -> mterrs) -> [|| makeTZ16Error . over each UnsafeMText <$> mterrs ||]
  Failure failures -> Code $ fail . toString $ unlines failures

collectErrors :: Contract st pt vd -> [(Text, Text)]
collectErrors contract = catMaybes $ definitionsList <&> \(SomeDocDefinitionItem item) ->
  cast item <&> \(DError (Proxy :: Proxy e)) ->
    (errorDocName @e, fmt $ errorDocMdCause @e)
  where
    definitionsList = toList . cdDefinitionsSet . buildDoc $ finalizedAsIs contract

validateErrors :: [(Text, Text)] -> Validation [Text] [(MText, MText)]
validateErrors errs = for errs \(err, desc) ->
  (,) <$> validateMText err err <*> validateMText (err <> " description") desc
  where
    validateMText loc = liftError (\msg -> ["In " <> loc <>": " <> msg]) . mkMText

makeTZ16Error :: (MText, MText) -> Error
makeTZ16Error (err, desc) =
  EStatic StaticError
    { seError = toExpression . toVal $ err
    , seExpansion = toExpression . toVal $ desc
    , seLanguages = ["en-US"]
    }
