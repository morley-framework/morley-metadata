-- SPDX-FileCopyrightText: 2023 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Morley.Metadata.Util.CallView
  ( callOffChainView
  , ViewParam(..)

  -- * Errors
  , MetadataError(..)
  , MichelsonStorageViewTypeCheckError(..)

  -- * Internals
  , callOffChainViewAbstract
  , OffChainCallMonadDict(..)
  ) where

import Data.Singletons (demote)
import Fmt (Buildable(build), nameF, pretty, unlinesF, (+|), (|+))
import Lorentz (NiceParameter, NiceStorage, ToT, def, zeroMutez)
import Lorentz qualified as L
import Morley.AsRPC
import Morley.Client (MorleyClientM)
import Morley.Client qualified as Client
import Morley.Client.Util qualified as Client
import Morley.Micheline (Expression, FromExpression, FromExpressionError, fromExpression)
import Morley.Michelson.Typed
  (Dict(..), Instr(..), IsoValue, T, mkContractCode, pattern (:#), starNotes, starParamNotes, toVal,
  untypeValue)
import Morley.Michelson.Typed qualified as T
import Morley.Michelson.Untyped qualified as U
import Morley.Tezos.Address (ContractAddress)
import Morley.Util.Interpolate

import Lorentz.Contracts.Spec.TZIP16Interface

-- | A parameter to be passed to a view.
data ViewParam (viewParamMaybe :: Maybe Type) where
  ViewParam :: forall viewParam. NiceParameter viewParam => viewParam -> ViewParam ('Just viewParam)
  NoParam :: ViewParam 'Nothing

-- | If a view has a parameter, then its instructions should
-- expect a @(viewParam, storage)@ tuple at the top of the stack.
--
-- If it doesn't, then they should expect a @storage@ at the top of the stack.
type ViewInstrParam :: Maybe Type -> Type -> Type
type family ViewInstrParam viewParamMaybe storage where
  ViewInstrParam ('Just viewParam) storage = (viewParam, storage)
  ViewInstrParam 'Nothing storage = storage

data OffChainCallMonadDict m = OffChainCallMonadDict
  { occmdThrow :: forall e a. (Exception e, Buildable e) => e -> m a
  , occmdRunCode
      :: forall cp st
       . ( NiceStorage st, HasRPCRepr st, IsoValue (AsRPC st), NiceParameter cp)
      => L.Contract cp (Maybe st) ()
      -> U.Value
      -> m (Maybe (AsRPC st))
  }

-- | Call a TZIP-16-compliant off-chain view using morley-client.
--
-- Limitations:
--
--   * Only metadata URIs pointing to the same contract (e.g. @tezos-storage:metadata-key@)
--     are supported for now.
--   * View's @version@ field is not supported.
--   * If a view has many implementations, only the first "Michelson storage view" is tried.
callOffChainView
  :: forall viewRet viewParamMaybe storage
   . ( HasRPCRepr storage
     , IsoValue (AsRPC storage)
     , HasRPCRepr viewRet
     , IsoValue (AsRPC viewRet)
     , NiceStorage viewRet
     , NiceStorage storage
     )
  => [View (ToT storage)]
  -> AsRPC storage
  -> ContractAddress
  -> Text
  -> ViewParam viewParamMaybe
  -> MorleyClientM (AsRPC viewRet)
callOffChainView vs st addr = callOffChainViewAbstract @viewRet @_ @storage dict vs st
  where
    dict = OffChainCallMonadDict
      { occmdThrow = throwM
      , occmdRunCode = \(contract :: L.Contract cp (Maybe st) vd) param -> do
          balance <- Client.getBalance addr
          T.fromVal <$> Client.runContract Client.RunContractParameters
            { rcpContract = L.toMichelsonContract $ contract
            , rcpParameter = param
            , rcpStorage = untypeValue $ toVal $ Nothing @st
            , rcpAmount = zeroMutez
            , rcpBalance = balance
            , rcpSource = Nothing
            , rcpLevel = Nothing
            , rcpNow = Nothing
            , rcpSender = Nothing
            }
      }

-- | Call a TZIP-16-compliant off-chain view.
--
-- Limitations:
--
--   * Only metadata URIs pointing to the same contract (e.g. @tezos-storage:metadata-key@)
--     are supported for now.
--   * View's @version@ field is not supported.
--   * If a view has many implementations, only the first "Michelson storage view" is tried.
callOffChainViewAbstract
  :: forall viewRet viewParamMaybe storage m
   . ( HasRPCRepr storage
     , IsoValue (AsRPC storage)
     , HasRPCRepr viewRet
     , IsoValue (AsRPC viewRet)
     , NiceStorage viewRet
     , NiceStorage storage
     , Monad m
     )
  => OffChainCallMonadDict m
  -> [View (ToT storage)]
  -> AsRPC storage
  -> Text
  -> ViewParam viewParamMaybe
  -> m (AsRPC viewRet)
callOffChainViewAbstract OffChainCallMonadDict{..} offChainViews st viewName viewParam = do
  -- Note: this is an implementation of the guidelines outlined here:
  -- https://gitlab.com/tezos/tzip/-/issues/45
  --
  -- In short:
  -- We have a contract with storage of type `storage`
  -- and an off-chain view of type `(viewParam, storage) -> viewRet`.
  --
  -- We create (but don't originate) a contract that, when passed a `(viewParam,
  -- storage)`, will run the view's instructions and then save the view's return
  -- value of type `viewRet` in that contract's storage.
  --
  -- We then ask the RPC's `/run_code` to run that contract for us and return
  -- its final storage.
  offChainView <- findView viewName offChainViews
    & evalJust occmdThrow (MEViewNotFound viewName)

  offChainViewImpl <- either occmdThrow pure $ findViewImplementation @storage viewName offChainView

  Dict <- pure $ rpcHasNoOpEvi @(ToT storage)
  Dict <- pure $ viewInstrParamDict @storage viewParam

  instr <- typeCheckOffChainView @storage @viewRet viewParam offChainViewImpl
    & evalRight occmdThrow (METypeCheckingFailed viewName)

  let contract :: L.Contract (ViewInstrParam viewParamMaybe storage) (Maybe viewRet) () =
        L.Contract
          { cMichelsonContract = T.Contract
            { cCode = mkContractCode $
                T.CAR
                :# instr
                :# SOME
                :# NIL
                :# PAIR
            , cParamNotes = starParamNotes
            , cStoreNotes = starNotes
            , cViews = T.emptyViewsSet
            , cEntriesOrder = def
            }
          , cDocumentedCode = L.mkContractCode L.fakeCoerce
          }

  occmdRunCode
    contract
    case viewParam of
      -- this is a bit of a cheat: st is `AsRPC storage`, but we expect
      -- `storage`, hence the untyping. This should be fine for our purposes, as
      -- we're running via RunCode which can deal with this.
      ViewParam vp -> untypeValue $ toVal (vp, st)
      NoParam -> untypeValue $ toVal st
    >>= evalJust occmdThrow (MEInternalError viewName "Expected contract's storage to be 'Some', but it was 'None'.")


-- | Find a view in the contract's metadata by its name.
findView :: Text -> [View st] -> Maybe (View st)
findView viewName = find $ (== viewName) . vName

findViewImplementation
  :: forall storage. Text -> View (ToT storage) -> Either MetadataError (MichelsonStorageView (ToT storage))
findViewImplementation viewName view_ = maybeToRight (MEImplementationNotFound viewName) $
  getFirst $ foldMap (First . isMichelsonStorageView @storage) $ vImplementations view_

isMichelsonStorageView :: ViewImplementation (ToT storage) -> Maybe (MichelsonStorageView (ToT storage))
isMichelsonStorageView = \case
  VIMichelsonStorageView msv -> Just msv
  _ -> Nothing

evalJust :: Applicative m => (MetadataError -> m a) -> MetadataError -> Maybe a -> m a
evalJust doThrow err = maybe (doThrow err) pure

----------------------------------------------------------------------------
-- Helpers
----------------------------------------------------------------------------

evalRight :: Applicative m => (MetadataError -> m b) -> (a -> MetadataError) -> Either a b -> m b
evalRight doThrow mkErr = either (doThrow . mkErr) pure

-- | Construct a proof that there is a `NiceParameter` dict for the view's parameter.
viewInstrParamDict
  :: forall storage viewParamMaybe
  .  NiceStorage storage
  => ViewParam viewParamMaybe
  -> Dict (NiceParameter (ViewInstrParam viewParamMaybe storage))
viewInstrParamDict = \case
  ViewParam {} -> Dict
  NoParam {} -> Dict

typeCheckOffChainView
  :: forall st ret viewParamMaybe
   . (IsoValue st, IsoValue ret)
  => ViewParam viewParamMaybe
  -> MichelsonStorageView (ToT st)
  -> Either MichelsonStorageViewTypeCheckError (Instr '[ ToT (ViewInstrParam viewParamMaybe st) ] '[ ToT ret ])
typeCheckOffChainView viewParam (MichelsonStorageView paramTypeExprMaybe retTypeExpr code _ _) = do
  -- Note: we don't actually *need* to perform these typechecks here, we
  -- could just try to decode the expression to a typed `Instr` and see it if succeeds.
  -- But the decoding error message could potentially be very confusing.
  --
  -- So, it's best to typecheck `param` and `ret` first and, if they fail, return user-friendly
  -- error messages. If they succeed, _then_ we decode the expression to a typed `Instr`.
  typeCheckReturnType @ret retTypeExpr

  case viewParam of
    ViewParam (_ :: viewParam) -> do
      case paramTypeExprMaybe of
        Nothing -> Left $ MSVTCEParamExpected (demote @(ToT viewParam))
        Just paramTypeExpr -> do
          paramType <- fromExpressionEither @T "parameter" paramTypeExpr
          when (demote @(ToT viewParam) /= paramType) $
            Left $ MSVTCEParamTypeMismatch (demote @(ToT viewParam)) paramType
      fromExpressionEither @(Instr '[ ToT (viewParam, st) ] '[ ToT ret ]) "code" code

    NoParam {} -> do
      whenJust paramTypeExprMaybe \paramTypeExpr ->
        Left $ MSVTCEParamNotExpected paramTypeExpr
      fromExpressionEither @(Instr '[ ToT st ] '[ ToT ret ]) "code" code

typeCheckReturnType :: forall ret. IsoValue ret => Expression -> Either MichelsonStorageViewTypeCheckError ()
typeCheckReturnType retTypeExpr = do
  retType <- fromExpressionEither @T "return-type" retTypeExpr
  when (demote @(ToT ret) /= retType) $
    Left $ MSVTCEReturnTypeMismatch (demote @(ToT ret)) retType

fromExpressionEither :: FromExpression a => Text -> Expression -> Either MichelsonStorageViewTypeCheckError a
fromExpressionEither fieldName = first (MSVTCEDecodingError fieldName) . fromExpression

data MichelsonStorageViewTypeCheckError
  = MSVTCEParamExpected T
  | MSVTCEParamNotExpected Expression
  | MSVTCEParamTypeMismatch T T
  | MSVTCEReturnTypeMismatch T T
  | MSVTCEDecodingError Text FromExpressionError
  deriving stock (Eq, Show)

instance Buildable MichelsonStorageViewTypeCheckError where
  build = \case
    MSVTCEParamExpected expectedT -> unlinesF
      [ nameF "Expected the view's parameter type to be" $ build expectedT
      , "But the view does not have a parameter."
      ]
    MSVTCEParamNotExpected actualT ->
      nameF "Expected the view to not have a parameter, but it did" $ build actualT
    MSVTCEParamTypeMismatch expectedT actualT -> unlinesF
      [ nameF "Expected the view's parameter type to be" $ build expectedT
      , nameF "But it was" (build actualT)
      ]
    MSVTCEReturnTypeMismatch expectedT actualT -> unlinesF
      [ nameF "Expected the view's return type to be" $ build expectedT
      , nameF "But it was" $ build actualT
      ]
    MSVTCEDecodingError fieldName err -> unlinesF
      [ "Failed to decode '" +| fieldName |+ "'."
      , nameF "Reason" $ build err
      ]

data MetadataError
  = MEViewNotFound Text
  | MEImplementationNotFound Text
  | METypeCheckingFailed Text MichelsonStorageViewTypeCheckError
  | MEInternalError Text Text
  deriving stock Show

instance Exception MetadataError where
  displayException = pretty

instance Buildable MetadataError where
  build = \case
    MEImplementationNotFound viewName ->
      [itu|
        A 'Michelson Storage View' implementation was not found for off-chain view '#{viewName}'.
        Note that 'Rest API Query' implementations are not supported at the moment.
        |]
    MEViewNotFound viewName ->
      [itu|Metadata does not contain off-chain view with name: '#{viewName}'.|]
    METypeCheckingFailed viewName err ->
      [itu|
        Failed to typecheck off-chain view '#{viewName}':
          #{err}
        |]
    MEInternalError viewName err ->
      [itu|
        Internal error while evaluating off-chain view '#{viewName}', this is most likely a bug:
          #{err}
        |]
