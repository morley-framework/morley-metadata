-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# LANGUAGE DeriveLift #-}

module Morley.Metadata
  (
  -- * Constructing a view's code
    compileViewCode
  , unsafeCompileViewCode
  , compileViewCodeTH
  , ViewCode(.., WithParam, WithoutParam)
  , CompiledViewCode -- NOTE: the constructor is purposefully not exported
  , ViewCodeError(..)

  -- * Constructing a view implementation
  , mkSimpleMichelsonStorageView
  , mkMichelsonStorageView

  -- * Internals
  , compileViewCode'
  , validateViewCode
  ) where

import Control.Lens (universe)
import Data.Either.Validation (Validation(Failure, Success), validationToEither)
import Fmt (Buildable(..), blockListF, indentF, pretty, unlinesF)
import Language.Haskell.TH.Syntax as TH (Code(Code), Lift, Q, liftTyped)

import Lorentz
  (CompilationOptions, Fn, FollowEntrypointFlag(NotFollowEntrypoint), HasAnnotation(getAnnotation),
  IsoValue(..), compileLorentzWithOptions, defaultCompilationOptions, unpair, (#), (:->))
import Morley.Micheline
  (Exp(..), Expression, MichelinePrimAp(..), MichelinePrimitive(..), toExpression)
import Morley.Michelson.Typed.Contract (IsNotInView, giveNotInView)

import Lorentz.Contracts.Spec.TZIP16Interface

----------------------------------------------------------------------------
-- Constructing a view's code
----------------------------------------------------------------------------

-- | A view's code, which may or may not take an additional parameter.
--
-- In general, use 'WithParam' and 'WithoutParam' to construct 'ViewCode'. When
-- pattern-matching with those, code will ask for 'IsNotInView' constraint,
-- however. Note this "not in view" refers to on-chain views, not metadata
-- views. If you don't have that constraint handy, use 'UnsafeWithParam' and
-- 'UnsafeWithoutParam' instead.
data ViewCode st ret where
  UnsafeWithParam :: (HasAnnotation param, IsoValue param) => '[param, st] :-> '[ret] -> ViewCode st ret
  UnsafeWithoutParam :: Fn st ret -> ViewCode st ret

pattern WithParam
  :: forall {st} {ret}. ()
  => forall param. (HasAnnotation param, IsoValue param)
  => (IsNotInView => '[param, st] :-> '[ret]) -> ViewCode st ret
pattern WithParam code <- UnsafeWithParam code
  where WithParam code = UnsafeWithParam $ giveNotInView code

pattern WithoutParam :: (IsNotInView => Fn st ret) -> ViewCode st ret
pattern WithoutParam code <- UnsafeWithoutParam code
  where WithoutParam code = UnsafeWithoutParam $ giveNotInView code

{-# COMPLETE WithParam, WithoutParam #-}

-- | A view's code, compiled and guaranteed to not contain any of the instructions
-- forbidden by the TZIP-16 spec, which may take as a parameter:
--
-- 1. The contract's storage, or
-- 2. An additional parameter and the contract's storage.
data CompiledViewCode st ret = CompiledViewCode
  { vcCodeExpression :: Expression
  , vcParamExpression :: Maybe Expression
  }
  deriving stock (Lift, Show)

-- | An error raised during the compilation of a view's code.
newtype ViewCodeError = InvalidInstructions (NonEmpty Text)
  deriving stock (Eq, Show)

instance Exception ViewCodeError where
  displayException = pretty

instance Buildable ViewCodeError where
  build (InvalidInstructions instrs) = unlinesF
    [ "The view's code contains instructions forbidden by the TZIP-16 specification:"
    , indentF 8 $ blockListF (toList instrs)
    ]

-- | Compile a view's code with the given Lorentz compilation options.
-- Mainly used for tests.
compileViewCode' :: forall st ret. CompilationOptions -> ViewCode st ret -> Either ViewCodeError (CompiledViewCode st ret)
compileViewCode' opts code =
  validateViewCode codeExpr $> CompiledViewCode
    { vcCodeExpression = codeExpr
    , vcParamExpression = paramExpr
    }
  where
    codeExpr =
      case code of
        UnsafeWithParam c -> toExpression $ compileLorentzWithOptions opts $ unpair # c
        UnsafeWithoutParam c -> toExpression $ compileLorentzWithOptions opts c
    paramExpr =
      case code of
        UnsafeWithoutParam _ -> Nothing
        UnsafeWithParam (_ :: '[param, st] :-> '[ret]) ->
          Just $ toExpression (getAnnotation @param NotFollowEntrypoint)

-- | Compiles a view's code.
--
-- If the view's code contains any of the instructions forbidden by the
-- TZIP-16 spec, a 'ViewCodeError' will be returned.
compileViewCode :: forall st ret. ViewCode st ret -> Either ViewCodeError (CompiledViewCode st ret)
compileViewCode = compileViewCode' defaultCompilationOptions

-- | Compiles a view's code.
--
-- If the view's code contains any of the instructions forbidden by the
-- TZIP-16 spec, an error will be thrown.
unsafeCompileViewCode :: HasCallStack => forall st ret. ViewCode st ret -> CompiledViewCode st ret
unsafeCompileViewCode = either (error . pretty) id . compileViewCode

-- | Construct a view's code.
--
-- If the view's code contains any of the instructions forbidden by the
-- TZIP-16 spec, a compiler error will be raised.
--
-- Usage:
--
-- > mkSimpleMichelsonStorageView @Storage @Integer $
-- >   $$(compileViewCodeTH $ WithParam @Integer $
-- >       dip (toField #sField2) # add
-- >     )
compileViewCodeTH :: forall st ret. ViewCode st ret -> Code Q (CompiledViewCode st ret)
compileViewCodeTH code =
  case compileViewCode code of
    Left err -> Code $ fail $ pretty err
    Right cvc -> TH.liftTyped cvc

-- | Checks whether a view's code contains any instructions forbidden by the TZIP-16 spec.
--
-- See: <https://gitlab.com/tzip/tzip/-/blob/72b9505579b4b5ebc4b49b87aa390301af062719/proposals/tzip-16/tzip-16.md#michelson-storage-views>
validateViewCode :: Expression -> Either ViewCodeError ()
validateViewCode expr =
  first InvalidInstructions . validationToEither $
    for_ (universe expr) $ \case
      ExpPrim _ (MichelinePrimAp prim _ _) | prim `elem` forbiddenInstructions ->
        Failure $ one $ pretty prim
      _ -> Success ()
  where
    forbiddenInstructions :: [MichelinePrimitive]
    forbiddenInstructions =
      [ Prim_AMOUNT
      , Prim_CREATE_CONTRACT
      , Prim_SENDER
      , Prim_SET_DELEGATE
      , Prim_SOURCE
      , Prim_TRANSFER_TOKENS
      ]

----------------------------------------------------------------------------
-- Constructing a view implementation
----------------------------------------------------------------------------

-- | Create an off-chain view implementation with no annotation descriptions.
mkSimpleMichelsonStorageView
  :: forall st ret
   . HasAnnotation ret
  => CompiledViewCode st ret -> MichelsonStorageView (ToT st)
mkSimpleMichelsonStorageView =
  mkMichelsonStorageView Nothing []

-- | Create an off-chain view implementation with the given annotation descriptions.
-- If a michelson version is supplied, this implementation will be compatible with said version.
mkMichelsonStorageView
  :: forall st ret
   . HasAnnotation ret
  => Maybe MichelsonVersion -> [AnnotationInfo] -> CompiledViewCode st ret -> MichelsonStorageView (ToT st)
mkMichelsonStorageView mversion anns (CompiledViewCode codeExpr paramExpr) =
  MichelsonStorageView
    { msvParameter = paramExpr
    , msvReturnType = toExpression (getAnnotation @ret NotFollowEntrypoint)
    , msvCode = codeExpr
    , msvAnnotations = anns
    , msvVersion = mversion
    }
