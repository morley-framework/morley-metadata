-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- | TZIP-16 specification.
module Test.Lorentz.Contracts.Spec.TZIP16Interface.URI
  ( test_URIs
  ) where

import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase, (@?=))

import Lorentz.Value
import Morley.Tezos.Address

import Lorentz.Contracts.Spec.TZIP16Interface
import Morley.Michelson.Typed.Haskell.Value

test_URIs :: [TestTree]
test_URIs =
  [ testGroup "Path pieces"
      [ testCase "Can construct simple path" $
          uriEncodePathSegment "abc" @?= "abc"
      , testCase "Can construct empty path" $
          uriEncodePathSegment "" @?= ""
      , testCase "Slashes are escaped" $
          uriEncodePathSegment "/abc/def" @?= "%2Fabc%2Fdef"
      , testCase "Colons are not escaped" $
          uriEncodePathSegment "abc:def" @?= "abc:def"
      ]

  , testGroup "tezos-storage schema"
      [ testCase "For local contract" $
          tezosStorageUri selfHost [mt|abc|]
            @?= "tezos-storage:abc"
      , testCase "For some contract" $
          let addr = [ta|KT1QDFEu8JijYbsJqzoXq7mKvfaQQamHD1kX|]
          in tezosStorageUri (foreignContractHost "mainnet" addr) mempty
            @?= "tezos-storage://KT1QDFEu8JijYbsJqzoXq7mKvfaQQamHD1kX.mainnet/"
      ]

  , testCase "Can include URI in metadata" $
      (bmMap $ metadataURI ("http://nyan.cat" :: URI))
        @?= (one ([mt||], "http://nyan.cat"))
  ]
