-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Lorentz.Contracts.Spec.TZIP16Interface
  ( test_roundtrip
  , test_Metadata_adheres_to_schema
  , unit_MichelsonStorageView_serializes_correctly
  , unit_RestApiQuery_serializes_correctly
  , unit_StaticError_serializes_correctly
  , unit_DynamicError_serializes_correctly
  , unit_Empty_arrays_toplevel_fields_are_omitted
  , unit_Empty_source_tools_are_omitted
  , unit_Empty_static_error_languages_are_omitted
  , unit_Empty_dynamic_error_languages_are_omitted
  , unit_Empty_storage_annotations_are_omitted
  ) where

import Data.Aeson as J (ToJSON(toJSON), Value, eitherDecode, encode, object, (.=))
import Data.Aeson.QQ (aesonQQ)
import Data.Aeson.TH (deriveJSON)
import Data.Aeson.Text (encodeToLazyText)
import Hedgehog (Gen, annotate, evalIO, failure, forAll, property, tripping, withTests)
import Network.HTTP.Simple (getResponseBody, httpJSON, parseRequest, setRequestBodyJSON)
import Test.Tasty (TestName, TestTree)
import Test.Tasty.HUnit (Assertion, (@?=))
import Test.Tasty.Hedgehog (testProperty)

import Lorentz as L hiding (Value, source)
import Morley.Micheline (ToExpression(toExpression))

import Lorentz.Contracts.Spec.TZIP16Interface
import Morley.Metadata (ViewCode(..), compileViewCodeTH, mkMichelsonStorageView)
import Morley.Metadata.Util.Aeson (aesonOptions)

import Hedgehog.Gen.Metadata qualified as Gen
import Test.Util (Param, Return, Storage)

test_roundtrip :: [TestTree]
test_roundtrip =
  [ roundtripMetadataField "name" name (getName >=> getJust)
      Gen.smallText
  , roundtripMetadataField "description" description (getDescription >=> getJust)
      Gen.smallText
  , roundtripMetadataField "version" version (getVersion >=> getJust)
      Gen.smallText
  , roundtripMetadataField "license" license (getLicense >=> getJust)
      Gen.genLicense
  , roundtripMetadataField "authors" authors getAuthors
      (Gen.smallList Gen.genAuthor)
  , roundtripMetadataField "homepage" homepage (getHomepage >=> getJust)
      Gen.smallText
  , roundtripMetadataField "source" source (getSource >=> getJust)
      Gen.genSource
  , roundtripMetadataField "interfaces" interfaces getInterfaces
      (Gen.smallList Gen.genInterface)
  , roundtripMetadataField "errors" errors getErrors
      (Gen.smallList Gen.genError)
  , roundtripMetadataField "views" views getViews
      (Gen.smallList Gen.genView)
  ]
  where
    roundtripMetadataField
      :: (Show a, Eq a)
      => TestName
      -> (a -> Metadata (ToT Storage))
      -> (Metadata (ToT Storage) -> Either String a)
      -> Gen a
      -> TestTree
    roundtripMetadataField testName to from gen =
      testProperty testName $ property $ do
        a <- forAll gen
        tripping a
          (encode . to)
          (eitherDecode >=> from)

    getJust :: Maybe a -> Either String a
    getJust = maybeToRight "Expected 'Just', got 'Nothing"

data ValidationResult = ValidationResult
  { vrValid :: Bool
  , vrErrors :: [Text]
  }
deriveJSON aesonOptions ''ValidationResult

test_Metadata_adheres_to_schema :: IO TestTree
test_Metadata_adheres_to_schema = do
  getMetadataSchema <&> \schema ->
    testProperty "Metadata adheres to schema" $ withTests 5 $ property $ do
      md <- toJSON <$> forAll Gen.genMetadata
      result <- evalIO $ validateAgainstSchema md schema
      unless (vrValid result) $ do
        annotate $ toString $ encodeToLazyText md
        traverse_ (annotate . toString) (vrErrors result)
        failure
  where
    getMetadataSchema :: IO Value
    getMetadataSchema = do
      request <- parseRequest "https://gitlab.com/tzip/tzip/-/raw/27e556c0ca7f13acbf0eacea354f426447f02f18/proposals/tzip-16/metadata-schema.json"
      getResponseBody <$> httpJSON request

    validateAgainstSchema :: Value -> Value -> IO ValidationResult
    validateAgainstSchema metadata schema = do
      let body = object
            [ "schema" .= schema
            , "json" .= metadata
            ]
      -- See: https://assertible.com/json-schema-validation
      request <- setRequestBodyJSON body <$> parseRequest "POST https://assertible.com/json"
      getResponseBody <$> httpJSON request

unit_MichelsonStorageView_serializes_correctly :: Assertion
unit_MichelsonStorageView_serializes_correctly = do
  let testView :: View (ToT Storage)
      testView = View
        { vName = "GetCounter"
        , vDescription = Just "Some description"
        , vPure = Just True
        , vImplementations = one $
            VIMichelsonStorageView $
              mkMichelsonStorageView @Storage @Return
                (Just edoVersion)
                [AnnotationInfo "paramField1" "some-ann-desc"]
                $$(compileViewCodeTH $ WithParam @Param $ L.drop # toField #sField2 # dup # pair # forcedCoerce_)
        }
  toJSON testView @?=
    [aesonQQ|
      {
        "name": "GetCounter",
        "pure": true,
        "description": "Some description",
        "implementations": [
          {
            "michelsonStorageView": {
              "annotations": [
                { "name": "paramField1", "description": "some-ann-desc" }
              ],
              "returnType": {
                "prim":"pair",
                "args": [
                  {"prim":"nat", "annots": ["%retField1"]},
                  {"prim":"nat", "annots": ["%retField2"]}
                ]
              },
              "parameter": {
                "prim":"pair",
                "args": [
                  {"prim":"int", "annots": ["%paramField1"]},
                  {"prim":"nat", "annots": ["%paramField2"]}
                ]
              },
              "code": [
                {"prim": "CDR"},
                {"prim": "CDR"},
                {"prim": "DUP"},
                {"prim": "PAIR"}
              ],
              "version": "PtEdoTezd3RHSC31mpxxo1npxFjoWWcFgQtxapi51Z8TLu6v6Uq"
            }
          }
        ]
      }
    |]

unit_RestApiQuery_serializes_correctly :: Assertion
unit_RestApiQuery_serializes_correctly = do
  let testView :: View (ToT Storage)
      testView = View
        { vName = "GetCounter"
        , vDescription = Just "Some description"
        , vPure = Just True
        , vImplementations = one $
            VIRestApiQuery RestApiQuery
              { ravSpecificationUri = "https://localhost"
              , ravBaseUri = Just "https://google.com"
              , ravPath = "/endpoint"
              , ravMethod = Just GET
              }
        }
  toJSON testView @?=
    [aesonQQ|
      {
        "name": "GetCounter",
        "pure": true,
        "description": "Some description",
        "implementations": [
          {
            "restApiQuery": {
              "specificationUri": "https://localhost",
              "baseUri": "https://google.com",
              "path": "/endpoint",
              "method": "GET"
            }
          }
        ]
      }
    |]

unit_StaticError_serializes_correctly :: Assertion
unit_StaticError_serializes_correctly = do
  let err = EStatic StaticError
        { seError = toExpression $ toVal @Natural 2
        , seExpansion = toExpression $ toVal @MText [mt|hello|]
        , seLanguages = ["pt-PT"]
        }
  toJSON err @?=
    [aesonQQ|
      {
        "error": {"int": "2"},
        "expansion": {"string": "hello"},
        "languages": ["pt-PT"]
      }
    |]

unit_DynamicError_serializes_correctly :: Assertion
unit_DynamicError_serializes_correctly = do
  let err = EDynamic DynamicError
        { deView = "some-view"
        , deLanguages = ["en-US"]
        }
  toJSON err @?=
    [aesonQQ|
      {
        "view": "some-view",
        "languages": ["en-US"]
      }
    |]

unit_Empty_arrays_toplevel_fields_are_omitted :: Assertion
unit_Empty_arrays_toplevel_fields_are_omitted = do
  let md = mconcat
        [ authors []
        , errors []
        , interfaces []
        , views []
        ]
  toJSON md @?= object []

unit_Empty_source_tools_are_omitted :: Assertion
unit_Empty_source_tools_are_omitted = do
  toJSON (Source (Just "git") []) @?=
    [aesonQQ|
      { "location": "git"}
    |]

unit_Empty_static_error_languages_are_omitted :: Assertion
unit_Empty_static_error_languages_are_omitted = do
  let err = EStatic $ StaticError
        { seError = toExpression $ toVal ()
        , seExpansion = toExpression $ toVal ()
        , seLanguages = []
        }
  toJSON err @?=
    [aesonQQ|
      {
        "error": {"prim":"Unit"},
        "expansion":{"prim":"Unit"}
      }
    |]

unit_Empty_dynamic_error_languages_are_omitted :: Assertion
unit_Empty_dynamic_error_languages_are_omitted = do
  let err = EDynamic $ DynamicError
        { deView = "some-view"
        , deLanguages = []
        }
  toJSON err @?=
    [aesonQQ|
      {
        "view": "some-view"
      }
    |]

unit_Empty_storage_annotations_are_omitted :: Assertion
unit_Empty_storage_annotations_are_omitted = do
  let msv = MichelsonStorageView
        { msvParameter = Nothing
        , msvReturnType = toExpression $ toVal ()
        , msvCode = toExpression $ toVal ()
        , msvAnnotations = []
        , msvVersion = Nothing
        }
  toJSON msv @?=
    [aesonQQ|
      {
        "returnType": {"prim":"Unit"},
        "code": {"prim":"Unit"}
      }
    |]
