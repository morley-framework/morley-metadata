-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Util
  ( Storage(..)
  , Param(..)
  , Return(..)
  )
  where

import Lorentz (HasAnnotation, IsoValue, MText)

-- | An example storage type used throughout the test suite.
-- It's defined in this module so it (and its instances) can be used in
-- Template Haskell splices (due to the GHC stage restriction).
data Storage = Storage
  { sField1 :: MText
  , sField2 :: Natural
  }
  deriving stock Generic
  deriving anyclass IsoValue

data Param = Param
  { paramField1 :: Integer
  , paramField2 :: Natural
  }
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

data Return = Return
  { retField1 :: Natural
  , retField2 :: Natural
  }
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)
