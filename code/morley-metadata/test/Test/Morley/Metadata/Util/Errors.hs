-- SPDX-FileCopyrightText: 2022 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# OPTIONS_GHC -fno-warn-warnings-deprecations -Wno-orphans #-}

module Test.Morley.Metadata.Util.Errors
  ( unit_Errors_fails_on_invalid_characters
  , unit_ErrorsTH_fails_on_invalid_characters
  , unit_Errors_collects_errors
  , unit_ErrorsTH_collects_errors
  ) where

import Prelude hiding (drop)

import Control.Exception (IOException)
import Language.Haskell.TH (examineCode, runQ)
import System.IO.Silently (hCapture)
import Test.Tasty.HUnit (Assertion, assertBool, assertFailure, (@?=))

import Lorentz
import Morley.Micheline (toExpression)
import Morley.Michelson.Text ()

import Lorentz.Contracts.Spec.TZIP16Interface (Error(..), StaticError(..))
import Morley.Metadata.Util.Errors (errorsFromAutodoc, errorsFromAutodocTH)

import Test.Morley.Metadata.Util.Errors.Contracts

unit_Errors_fails_on_invalid_characters :: Assertion
unit_Errors_fails_on_invalid_characters = case errorsFromAutodoc contractBad of
  Right _ -> assertFailure "Expected a failure, but got a success"
  Left msgs -> msgs @?= ["In TestingErrorBad description: Invalid character in string literal: \2361"]

unit_ErrorsTH_fails_on_invalid_characters :: Assertion
unit_ErrorsTH_fails_on_invalid_characters =
  hCapture [stderr] (try $ runQ $ examineCode (errorsFromAutodocTH contractBad)) >>= \case
    (_, Right _) -> assertFailure "Expected a failure, but got a success"
    (msg, Left (e :: IOException)) -> do
      displayException e @?= "user error (Q monad failure)"
      msg @?= "Template Haskell error: In TestingErrorBad description: Invalid character in string literal: \2361\n\n"

unit_Errors_collects_errors :: Assertion
unit_Errors_collects_errors = do
  case errorsFromAutodoc contractGood of
    Left err -> assertFailure $ "Expected success, but got " <> intercalate "; " (toString <$> err)
    Right errs -> do
      assertBool "Expected TestingErrorGood1 to be present" $ expectedError1 `elem` errs
      assertBool "Expected TestingErrorGood2 to be present" $ expectedError2 `elem` errs

expectedError1, expectedError2 :: Error
(expectedError2, expectedError1) =
  ( EStatic StaticError { seError = toExpression $ toVal [mt|TestingErrorGood2|]
                        , seExpansion = toExpression $ toVal [mt|Another error for testing|]
                        , seLanguages = ["en-US"] }
  , EStatic StaticError { seError = toExpression $ toVal [mt|TestingErrorGood1|]
                        , seExpansion = toExpression $ toVal [mt|An error for testing|]
                        , seLanguages = ["en-US"] }
  )

unit_ErrorsTH_collects_errors :: Assertion
unit_ErrorsTH_collects_errors = do
  let errs = $$(errorsFromAutodocTH contractGood)
  assertBool "Expected TestingErrorGood1 to be present" $ expectedError1 `elem` errs
  assertBool "Expected TestingErrorGood2 to be present" $ expectedError2 `elem` errs
