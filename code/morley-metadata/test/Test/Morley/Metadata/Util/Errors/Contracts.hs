-- SPDX-FileCopyrightText: 2022 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# OPTIONS_GHC -Wno-orphans #-}

-- | Contracts are defined here to work around the TH stage restriction.
module Test.Morley.Metadata.Util.Errors.Contracts
  ( contractGood
  , contractBad
  ) where

import Lorentz

contractBad :: Contract () () ()
contractBad = defaultContract $ failCustom_ #testingErrorBad

contractGood :: Contract () () ()
contractGood = defaultContract $ push True # if_ (failCustom_ #testingErrorGood1) (failCustom_ #testingErrorGood2)

[errorDocArg| "testingErrorBad" exception "An error for testing with invalid characters हि" () |]
[errorDocArg| "testingErrorGood1" exception "An error for testing" () |]
[errorDocArg| "testingErrorGood2" exception "Another error for testing" () |]
