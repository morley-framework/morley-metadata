-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

{-# OPTIONS_GHC -fno-warn-warnings-deprecations #-}

module Test.Morley.Metadata
  ( unit_Rejects_view_with_forbidden_instructions
  , unit_Rejects_view_with_nested_forbidden_instructions
  ) where

import Debug qualified
import Prelude hiding (drop)

import Data.List qualified as L
import Test.Hspec.Expectations (shouldMatchList)
import Test.Tasty.HUnit (Assertion, assertFailure)

import Lorentz

import Morley.Metadata
  (CompiledViewCode, ViewCode(..), ViewCodeError(..), compileViewCode, compileViewCode')


unit_Rejects_view_with_forbidden_instructions :: Assertion
unit_Rejects_view_with_forbidden_instructions =
  expectFailedDueToForbiddenInstructions ["AMOUNT", "CREATE_CONTRACT", "SENDER", "SET_DELEGATE", "SOURCE", "TRANSFER_TOKENS"] $
    compileViewCode' intactCompilationOptions $ WithoutParam viewCode
  where
    -- A view implementation that executes all the forbidden instructions.
    viewCode :: IsNotInView => Fn () ()
    viewCode =
      drop

      # amount
      # drop

      # unit # push zeroMutez # none @KeyHash
      # createContract @() @() (defaultContract failWith)
      # drop
      # drop

      # sender
      # drop

      # none @KeyHash # setDelegate
      # drop

      # source
      # drop

      # self # push [tz|1u|] # unit # transferTokens
      # drop

      # push ()

unit_Rejects_view_with_nested_forbidden_instructions :: Assertion
unit_Rejects_view_with_nested_forbidden_instructions =
  expectFailedDueToForbiddenInstructions ["AMOUNT"] $
    compileViewCode $ WithoutParam viewCode
  where
    viewCode :: Fn () Mutez
    viewCode =
      -- Execute forbidden instruction inside another "prim" expression (i.e. `dip`)
      (dip amount) # drop

expectFailedDueToForbiddenInstructions :: [Text] -> Either ViewCodeError (CompiledViewCode st ret) -> Assertion
expectFailedDueToForbiddenInstructions expectedInstrs = \case
  Right v -> assertFailure $ L.unlines
    [ "Expected view code to be invalid, but it was valid."
    , "View:"
    , Debug.show v
    ]
  Left (InvalidInstructions instrs) ->
    toList instrs `shouldMatchList` expectedInstrs
