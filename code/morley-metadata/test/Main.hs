-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Main
  ( main
  ) where

import Test.Cleveland.Ingredients (ourIngredients)
import Test.Tasty (defaultMainWithIngredients)

import Tree (tests)

main :: IO ()
main = tests >>= defaultMainWithIngredients ourIngredients
