-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Hedgehog.Gen.Metadata
  ( genMetadata
  , genLicense
  , genAuthor
  , genSource
  , genInterface
  , genError
  , genView

  -- * Helpers
  , smallList
  , smallText
  )
  where

import Control.Lens.Plated (rewrite)
import Hedgehog
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range

import Hedgehog.Gen.Morley.Micheline qualified as Gen
import Morley.Micheline (Exp(..), Expression)

import Lorentz.Contracts.Spec.TZIP16Interface

genMetadata :: Gen (Metadata st)
genMetadata =
  mconcat
    [ optField $ name <$> smallText
    , optField $ description <$> smallText
    , optField $ version <$> smallText
    , optField $ license <$> genLicense
    , optField $ authors <$> smallList genAuthor
    , optField $ homepage <$> smallText
    , optField $ source <$> genSource
    , optField $ interfaces <$> smallList genInterface
    , optField $ errors <$> smallList genError
    , optField $ views <$> smallList genView
    ]
  where
    optField :: Gen (Metadata st) -> Gen (Metadata st)
    optField gen = Gen.frequency
      [ (8, gen)
      , (2, pure mempty)
      ]

genLicense :: Gen License
genLicense = License
  <$> smallText
  <*> Gen.maybe smallText

genAuthor :: Gen Author
genAuthor = author <$> smallText <*> smallText

genSource :: Gen Source
genSource = Source
  <$> Gen.maybe smallText
  <*> smallList smallText

genInterface :: Gen Interface
genInterface =
  tzip <$> Gen.integral_ smallRange
  <|>
  tzipWithExtras <$> Gen.integral_ smallRange <*> smallText

genStaticError :: Gen StaticError
genStaticError = StaticError
  <$> genExpression
  <*> genExpression
  <*> smallList smallText

genDynamicError :: Gen DynamicError
genDynamicError = DynamicError
  <$> smallText
  <*> smallList smallText

genError :: Gen Error
genError =
  Gen.choice
    [ EStatic <$> genStaticError
    , EDynamic <$> genDynamicError
    ]

genView :: Gen (View st)
genView = View
  <$> smallText
  <*> Gen.maybe smallText
  <*> Gen.maybe Gen.bool
  <*> smallList genViewImplementation

genViewImplementation :: Gen (ViewImplementation st)
genViewImplementation =
  Gen.choice
    [ VIRestApiQuery <$> genRestApiQuery
    , VIMichelsonStorageView <$> genMichelsonStorageView
    ]

genRestApiQuery :: Gen RestApiQuery
genRestApiQuery = RestApiQuery
  <$> smallText
  <*> Gen.maybe smallText
  <*> smallText
  <*> Gen.maybe Gen.enumBounded

genMichelsonStorageView :: Gen (MichelsonStorageView st)
genMichelsonStorageView = MichelsonStorageView
  <$> Gen.maybe genExpression
  <*> genExpression
  <*> genExpression
  <*> smallList genAnnotationInfo
  <*> Gen.maybe (MichelsonVersion <$> smallText)

genAnnotationInfo :: Gen AnnotationInfo
genAnnotationInfo = AnnotationInfo
  <$> smallText
  <*> smallText

genExpression :: Gen Expression
genExpression =
  -- The current version of the TZIP-16 metadata has a bug in the pattern
  -- for micheline "bytes" expressions, where it
  -- requires the bytestring to have at least 1 byte.
  -- > "pattern": "^[a-zA-Z0-9]+$"
  --
  -- To work around this, we modify our expression generator to never generate
  -- empty `bytes` expressions.
  --
  -- TODO: remove this function when the bug in the schema is fixed.
  Gen.genExpression <&> rewrite \case
    ExpBytes ext "" -> Just $ ExpBytes ext "a"
    _ -> Nothing

----------------------------------------------------------------------------
-- Helpers
----------------------------------------------------------------------------

smallRange :: Integral a => Range a
smallRange = Range.linear 0 5

smallList :: Gen a -> Gen [a]
smallList = Gen.list smallRange

smallText :: Gen Text
smallText = Gen.text smallRange Gen.alphaNum
