-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Morley.Metadata
  ( ViewParam(..)
  , callOffChainView
  , getMetadata
  ) where

import Data.Aeson qualified as J
import Data.ByteString.Lazy qualified as BSL
import Data.Text qualified as Text
import Fmt (build, unlinesF, (+|), (|+))

import Morley.Michelson.Text (mkMText)
import Morley.Michelson.Typed (untypeValue)
import Morley.Michelson.Typed.AnnotatedValue (castTo, node, value)
import Morley.Michelson.Typed.Haskell.Value
import Test.Cleveland
import Test.Cleveland.Lorentz.Types

import Lorentz.Contracts.Spec.TZIP16Interface
import Morley.Metadata.Util.CallView hiding (callOffChainView)

-- | Call a TZIP-16-compliant off-chain view inside a Cleveland scenario.
--
-- Limitations:
--
--   * Only metadata URIs pointing to the same contract (e.g. @tezos-storage:metadata-key@)
--     are supported for now.
--   * View's @version@ field is not supported.
--   * If a view has many implementations, only the first "Michelson storage view" is tried.
callOffChainView
  :: forall viewRet viewParamMaybe cp storage vd caps m
   . ( HasCallStack
     , MonadCleveland caps m
     , HasRPCRepr storage
     , IsoValue (AsRPC storage)
     , HasRPCRepr viewRet
     , IsoValue (AsRPC viewRet)
     , NiceStorage viewRet
     )
  => ContractHandle cp storage vd
  -> Text
  -> ViewParam viewParamMaybe
  -> m (AsRPC viewRet)
callOffChainView addr viewName viewParam
  | Dict <- pickNiceStorage @storage addr = do
      metadata <- getMetadata addr
      offChainViews <- getViews metadata & evalRight \err -> "Views lookup failed: " <> build err
      st <- getStorage addr

      callOffChainViewAbstract @viewRet @_ @storage dict offChainViews st viewName viewParam
  where
    dict :: OffChainCallMonadDict m
    dict = OffChainCallMonadDict
      { occmdThrow = failure . build
      , occmdRunCode = \contract param -> do
          balance <- getBalance addr
          runCode RunCode
            { rcContract = contract
            , rcStorage = untypeValue $ toVal $ Nothing @viewRet
            , rcParameter = param
            , rcAmount = 0
            , rcBalance = balance
            , rcSource = Nothing
            , rcLevel = Nothing
            , rcNow = Nothing
            }
      }

-- | Retrieve a contract's TZIP-16 metadata.
getMetadata
  :: forall parameter storage vd caps m
   . (HasCallStack, MonadCleveland caps m)
  => ContractHandle parameter storage vd
  -> m (Metadata (ToT storage))
getMetadata addr = do
  storage <- getSomeStorage addr

  metadataMapUntyped <- storage ^? node "metadata"
    & evalJust "Storage does not contain a field with the field annotation 'metadata'."

  metadataMapId <- metadataMapUntyped ^? castTo @MetadataMapId . value
    & evalJust "The metadata field is not of type 'big_map string bytes'"

  uri <- getBigMapValueMaybe metadataMapId ""
    >>= evalJust "Metadata bigmap does not contain the key: ''"
    <&> decodeUtf8 @Text

  -- NOTE: We only support the `tezos-storage` scheme (for now).
  path <- Text.stripPrefix (tezosStorageScheme <> ":") uri
    & evalJust ("Expected 'tezos-storage' uri, but found: " <> build uri)

  -- NOTE: We only support tezos-storage URIs pointing to the current contract (for now).
  -- So we assume the path is simply the metadata key.
  metadataKey <- mkMText path
    & evalRight \err -> unlinesF
      [ "At the moment, only URIs pointing to the current contract are supported, e.g. 'tezos-storage:metadata-key'."
      , "The path of the given URI is not a valid michelson string."
      , "URI:   " <> build uri
      , "Path:  " <> build path
      , "Error: " <> build err
      ]

  metadataJSON <- getBigMapValueMaybe metadataMapId metadataKey
    >>= evalJust ("Metadata bigmap does not contain the key: '" +| metadataKey |+ "'")

  J.eitherDecode' @(Metadata (ToT storage)) (BSL.fromStrict metadataJSON)
    & evalRight build
