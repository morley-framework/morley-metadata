-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- The `deriveRPC "Storage"` function below generates an `HasRPC Storage` instance and a `StorageRPC` type.
-- The `HasRPC` instance is needed for the tests in `TestSuite.Morley.Metadata`,
-- but the `StorageRPC` type is reported as unused by GHC.
-- We use this flag to mute that warning.
{-# OPTIONS_GHC -Wno-unused-top-binds #-}

module TestSuite.Util
  ( idContract
  , Storage(..)
  , someMetadataMap
  , someMetadata
  ) where

import Data.Aeson qualified as J
import Data.ByteString.Lazy qualified as BSL
import Lorentz
import Lorentz qualified as L
import Morley.AsRPC
import Test.Cleveland.Instances ()
import Morley.Michelson.Typed.Haskell.Doc (FieldCamelCase)

import Lorentz.Contracts.Spec.TZIP16Interface
import Morley.Metadata

-- | Simple contract that does nothing when called.
idContract :: (NiceParameterFull cp, NiceStorageFull st) => Contract cp st ()
idContract = defaultContract $
  cdr #
  nil @Operation #
  pair

-- | Sample storage for a TZIP-16 compliant contract.
data Storage = Storage
  { sField1 :: Integer
  , sMetadata :: MetadataMap
  }
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)
  deriving TypeHasFieldNamingStrategy via FieldCamelCase

deriveRPC "Storage"

-- | Sample TZIP-16 compliant metadata.
someMetadataMap :: MetadataMap
someMetadataMap =
  mkBigMap
    [ ("", encodeURI "tezos-storage:metadata-goes-here")
    , ("metadata-goes-here", BSL.toStrict $ J.encode someMetadata )
    ]

someMetadata :: Metadata (ToT Storage)
someMetadata =
  mconcat
    [ name "awesome-contract"
    , views
        [ getField1
        , addField1
        , fetchLevel
        , multipleImpls
        ]
    ]

getField1 :: View (ToT Storage)
getField1 =
  View
    { vName = "get-field1"
    , vDescription = Just "Some description"
    , vPure = Just True
    , vImplementations = one . VIMichelsonStorageView $
        mkSimpleMichelsonStorageView @Storage $
          unsafeCompileViewCode $ WithoutParam $
              toField #sField1
    }

addField1 :: View (ToT Storage)
addField1 =
  View
    { vName = "add-field1"
    , vDescription = Just "Some description"
    , vPure = Just True
    , vImplementations = one . VIMichelsonStorageView $
        mkSimpleMichelsonStorageView @Storage $
          unsafeCompileViewCode $ WithParam @Integer $
              dip (toField #sField1) # add
    }

fetchLevel :: View (ToT Storage)
fetchLevel =
  View
    { vName = "fetch-level"
    , vDescription = Just "Some description"
    , vPure = Just True
    , vImplementations = one . VIMichelsonStorageView $
        mkSimpleMichelsonStorageView @Storage $
          unsafeCompileViewCode $ WithoutParam $
              L.drop # L.level
    }

multipleImpls :: View (ToT Storage)
multipleImpls =
  View
    { vName = "multiple-impls"
    , vDescription = Just "Some description"
    , vPure = Just True
    , vImplementations =
        [ VIRestApiQuery RestApiQuery
            { ravSpecificationUri = "https://invalid.uri"
            , ravBaseUri = Nothing
            , ravPath = ""
            , ravMethod = Nothing
            }
        , VIMichelsonStorageView $ mkSimpleMichelsonStorageView @Storage @Integer $ unsafeCompileViewCode $ WithoutParam $
            toField #sField1
        , VIMichelsonStorageView $ mkSimpleMichelsonStorageView @Storage @Integer $ unsafeCompileViewCode $ WithoutParam $
            failUsing @MText "third off-chain view implementation called"
        ]
    }
