-- SPDX-FileCopyrightText: 2022 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module TestSuite.Morley.Metadata
  ( test_getMetadata
  , test_callOffChainView
  , test_callOffChainView_with_env
  , test_callOffChainView_multiple_impls
  , test_callOffChainView_morley_client
  , test_callOffChainView_morley_client_with_env
  , test_callOffChainView_morley_client_multiple_impls
  ) where

import Data.Ix (inRange)
import Fmt (build)
import Test.Tasty (TestTree)

import Lorentz (IsoValue, NiceStorage, ToT)
import Morley.AsRPC (rpcStorageScopeEvi)
import Morley.Client qualified as Client
import Morley.Metadata.Util.CallView qualified as MorleyClient
import Morley.Michelson.Typed (Dict(..))
import Test.Cleveland
import Test.Cleveland.Internal.Abstract (MonadNetwork)
import Test.Cleveland.Lorentz.Types (pickNiceStorage)

import Lorentz.Contracts.Spec.TZIP16Interface (getViews)

import Test.Morley.Metadata
import TestSuite.Util

test_getMetadata :: TestTree
test_getMetadata =
  testScenario "getMetadata retrieves the contract's storage when it's stored directly in the contract" $ scenario do
    let storage = Storage
          { sField1 = 2
          , sMetadata = someMetadataMap
          }
    contract <- originate "contract" storage $ idContract @()
    getMetadata contract @@== someMetadata

test_callOffChainView :: TestTree
test_callOffChainView =
  testScenario "callOffChainView executes views" $ scenario do
    let storage = Storage
          { sField1 = 2
          , sMetadata = someMetadataMap
          }
    contract <- originate "contract" storage $ idContract @()

    callOffChainView @Integer contract "get-field1" NoParam @@== sField1 storage
    callOffChainView @Integer contract "add-field1" (ViewParam @Integer 3) @@== sField1 storage + 3

test_callOffChainView_with_env :: TestTree
test_callOffChainView_with_env =
  testScenario "callOffChainView executes views that depend on the current environment" $ scenario do
    let storage = Storage
          { sField1 = 2
          , sMetadata = someMetadataMap
          }
    contract <- originate "contract" storage $ idContract @()
    advanceLevel 1

    level0 <- getLevel
    levelResult <- callOffChainView @Natural contract "fetch-level" NoParam
    level1 <- getLevel

    checkCompares (level0, level1) inRange levelResult

test_callOffChainView_multiple_impls :: TestTree
test_callOffChainView_multiple_impls =
  testScenario "callOffChainView executes first 'michelson storage view' implementation" $ scenario do
    let storage = Storage
          { sField1 = 2
          , sMetadata = someMetadataMap
          }
    contract <- originate "contract" storage $ idContract @()

    callOffChainView @Integer contract "multiple-impls" NoParam @@== sField1 storage

callOffChainViewMC
  :: forall viewRet cp st vd viewParamMaybe caps m.
     (MonadNetwork caps m, NiceStorage viewRet, IsoValue (AsRPC st)
     , HasRPCRepr st, HasRPCRepr viewRet, IsoValue (AsRPC viewRet))
  => ContractHandle cp st vd
  -> Text
  -> ViewParam viewParamMaybe
  -> m (AsRPC viewRet)
callOffChainViewMC addr viewName param
  | Dict <- pickNiceStorage @st addr
  , Dict <- rpcStorageScopeEvi @(ToT st)
  = do
  env <- getMorleyClientEnv
  meta <- getMetadata addr
  vs <- getViews meta & evalRight \err -> "Views lookup failed: " <> build err
  stor <- getStorage @st addr
  runIO $ Client.runMorleyClientM env $
    MorleyClient.callOffChainView @viewRet @_ @st vs stor (toContractAddress addr) viewName param

test_callOffChainView_morley_client :: TestTree
test_callOffChainView_morley_client =
  testScenarioOnNetwork "callOffChainView_morley_client executes views" $ scenarioNetwork do
    let storage = Storage
          { sField1 = 2
          , sMetadata = someMetadataMap
          }
    contract <- originate "contract" storage $ idContract @()

    callOffChainViewMC @Integer contract "get-field1" NoParam @@== sField1 storage
    callOffChainViewMC @Integer contract "add-field1" (ViewParam @Integer 3) @@== sField1 storage + 3

test_callOffChainView_morley_client_with_env :: TestTree
test_callOffChainView_morley_client_with_env =
  testScenarioOnNetwork "callOffChainView_morley_client executes views that depend on the current environment" $ scenarioNetwork do
    let storage = Storage
          { sField1 = 2
          , sMetadata = someMetadataMap
          }
    contract <- originate "contract" storage $ idContract @()
    advanceLevel 1

    level0 <- getLevel
    levelResult <- callOffChainViewMC @Natural contract "fetch-level" NoParam
    level1 <- getLevel

    checkCompares (level0, level1) inRange levelResult

test_callOffChainView_morley_client_multiple_impls :: TestTree
test_callOffChainView_morley_client_multiple_impls =
  testScenarioOnNetwork "callOffChainView_morley_client executes first 'michelson storage view' implementation" $ scenarioNetwork do
    let storage = Storage
          { sField1 = 2
          , sMetadata = someMetadataMap
          }
    contract <- originate "contract" storage $ idContract @()

    callOffChainViewMC @Integer contract "multiple-impls" NoParam @@== sField1 storage
